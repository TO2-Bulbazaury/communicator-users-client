package communicator.users.client.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Jakub Janusz on 2015-12-08.
 */
public class CaesarEncryptionTest {

    CaesarEncryption caesarEncryption;

    @Before
    public void setUp(){
        caesarEncryption = new CaesarEncryption();
    }

    @Test
    public void testEncrypt() {
        String original = "aZ4(*";
        String encrypted = "_X2&(";
        assertEquals("testEncrypt", caesarEncryption.encrypt(original), encrypted);
    }

}