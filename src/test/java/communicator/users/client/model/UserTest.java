package communicator.users.client.model;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

/**
 * Created by Jakub Janusz on 2015-12-08.
 */
public class UserTest {

    String username;
    String password;
    Set<Preference> preferences;
    ProfileData profileData;
    User user;

    @Before
    public void setUp() {
        user = new User();
        preferences = new HashSet<>();
    }

    @Test
    public void testCreation() {
        assertNull("testCreation - username", user.getUsername());
        assertNull("testCreation - password", user.getPassword());
        assertEquals("testCreation - preferences", user.getPreferences(), preferences);
        assertNotNull("testCreation - profileData", user.getProfileData());
    }

    @Test
    public void testUsernameChange() {
        username = "firstUsername";
        user.setUsername(username);
        assertEquals("testUsernameChange - first", user.getUsername(), username);
        username = "secondUsername";
        assertNotEquals("testUsernameChange - second", user.getUsername(), username);
    }

    @Test
    public void testPasswordChange() {
        password = "firstPassword";
        user.setPassword(password);
        assertEquals("testPasswordChange - first", user.getPassword(), password);
        password = "secondPassword";
        assertNotEquals("testPasswordChange - second", user.getPassword(), password);
    }

    @Test
    public void testPreferencesChange() {
        Preference preference = mock(Preference.class);
        user.addPreference(preference);
        preferences.add(preference);
        assertEquals("testPreferencesChange", user.getPreferences(), preferences);
        Preference otherPreference = mock(Preference.class);
        user.addPreference(otherPreference);
        assertThat(user.getPreferences(), is(not(preferences)));
    }

    @Test
    public void testProfileDataChange() {
        profileData = mock(ProfileData.class);
        user.setProfileData(profileData);
        assertEquals("testProfileDataChange - first", user.getProfileData(), profileData);
        ProfileData otherProfileData = mock(ProfileData.class);
        assertThat("testProfileDataChange - second", user.getProfileData(), is(not(otherProfileData)));
    }

}