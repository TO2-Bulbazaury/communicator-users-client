package communicator.users.client.model;

/**
 * Created by Dawid on 2015-12-05.
 */
public interface Encryption {

    public String encrypt(String string);

}
