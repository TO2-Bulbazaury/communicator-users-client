package communicator.users.client.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Jakub Janusz on 2015-12-09.
 */
public class ServerSender extends Thread {

    private Socket server;
    private PrintWriter writer;
    private final String SEPARATOR = "#$#";
    private Logger logger;

    public ServerSender(Socket server) throws IOException {
        this.logger = LoggerFactory.getLogger(this.getClass().toString());
        this.server = server;
        try {
            this.writer = new PrintWriter(server.getOutputStream(), true);
        } catch (NullPointerException e) {
            logger.error(e.toString());
        }
    }

    public void run() {

    }

    public String appendUser(String request, User user) {
        request += user.getUsername();
        request += SEPARATOR;
        request += user.getPassword();

        return request;
    }

    public String appendProfileData(String request, ProfileData profileData) {
        request += profileData.getEmail();
        request += SEPARATOR;
        request += profileData.getName();
        request += SEPARATOR;
        request += profileData.getAge();
        request += SEPARATOR;
        request += profileData.getCity();

        return request;
    }

    /**
     * @param user user that request is in common with
     * @param token session token
     * @param type type of request defined by one uppercase character
        * A - authorize
        * R - register user account
        * D - delete user account
        * C - change password
        * U - get active users list
        * P - get some user's profile data
        * E - edit own profile data
        * X - deactivate own account
        * Y - activate own account after deactivation
        * B - ban some user (only administrator's privilege)
     * @param additionalInfo used only with C type as new password, E type as new profile data and B type as ban reason
     */
    public void sendRequest(User user, String token, String type, String additionalInfo) {
        String request = type + SEPARATOR;
        switch (type) {
            case "A":
                request = appendUser(request, user);
                writer.println(request);
                break;
            case "R":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                request += SEPARATOR;
                request = appendProfileData(request, user.getProfileData());
                writer.println(request);
                break;
            case "D":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                writer.println(request);
                break;
            case "C":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                request += SEPARATOR;
                request += additionalInfo;
                writer.println(request);
                break;
            case "U":
                request += token;
                request += SEPARATOR;
                request += user.getUsername();
                writer.println(request);
                break;
            case "P":
                request += token;
                request += SEPARATOR;
                request += user.getUsername();
                writer.println(request);
                break;
            case "E":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                request += SEPARATOR;
                request += additionalInfo;
                writer.println(request);
                break;
            case "X":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                writer.println(request);
                break;
            case "Y":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                writer.println(request);
                break;
            case "B":
                request += token;
                request += SEPARATOR;
                request = appendUser(request, user);
                request += SEPARATOR;
                request += additionalInfo;
                writer.println(request);
                break;
            default:
                logger.warn("sendRequest - Request type does not match any of possible types.");
                break;
        }
        logger.info("sendRequest - Sending following request to {}:\n\t{}", server, request);
    }

}
