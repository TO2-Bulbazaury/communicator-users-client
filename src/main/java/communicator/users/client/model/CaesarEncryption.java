package communicator.users.client.model;

/**
 * Created by Jakub Janusz on 2015-12-08.
 */
public class CaesarEncryption implements Encryption {

    public CaesarEncryption() {

    }

    public String encrypt(String string) {
        int n = string.length();
        char[] chars = new char[n];
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {
            chars[i] = string.charAt(i);
            chars[i] -= 2;
            sb.append(chars[i]);
        }

        String newString = sb.toString();

        return newString;
    }

}
