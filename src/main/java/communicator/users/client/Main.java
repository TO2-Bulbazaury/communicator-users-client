package communicator.users.client;

import communicator.users.client.controller.LoginController;
import communicator.users.client.controller.Overseer;
import communicator.users.client.persistence.HibernateUtils;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Communicator - main window");

        Overseer.getInstance().addController(new LoginController(primaryStage));
    }

    public static void main(String[] args) {
        HibernateUtils.session().close();
        launch(args);
    }

}

/*
import org.igniterealtime.restclient.RestApiClient;
import org.igniterealtime.restclient.entity.AuthenticationToken;
import org.igniterealtime.restclient.entity.UserEntities;

/**
 * Created by Jakub Janusz on 2016-01-13.

public class Main {

    public static void main(String[] args) {
        AuthenticationToken authenticationToken = new AuthenticationToken("admin", "projekt");

        RestApiClient restApiClient = new RestApiClient("10.20.128.210", 9090, authenticationToken);
        restApiClient.getUsers();
//        UserEntities users = restApiClient.getUsers();
//        System.out.println(users.toString());
    }

}
*/