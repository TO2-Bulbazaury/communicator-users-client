package communicator.users.client.controller;

import ch.qos.logback.classic.Logger;
import communicator.users.client.Main;
import communicator.users.client.model.Model;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class PasswordController implements Controller {
    private Stage primaryStage;
    private Stage passwordStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public PasswordController(){}
    public PasswordController(Stage primaryStage) {

    }

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Password.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            passwordStage = new Stage();
            passwordStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new PasswordController());
                }
            });
            passwordStage.setTitle("Password Options");
            passwordStage.initModality(Modality.WINDOW_MODAL);
            passwordStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            passwordStage.setScene(scene);


            passwordStage.showAndWait();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return passwordStage;
    }

    public void initialize(){
        errorLabelEmail.setTextFill(Color.RED);
        errorLabelPassword.setTextFill(Color.RED);
    }

    private boolean checkPass(String pass){
        return Model.getInstance().getEncryption().encrypt(pass).equals(Model.getInstance().getUser().getPassword());
    }

    @FXML
    private Button submitPassword;
    @FXML
    private Button submitEmail;
    @FXML
    private PasswordField oldPassField;
    @FXML
    private PasswordField newPassField;
    @FXML
    private PasswordField confNewPassField;
    @FXML
    private TextField newEmailField;
    @FXML
    private TextField confNewEmailField;

    @FXML
    private Label errorLabelPassword;
    @FXML
    private Label errorLabelEmail;

    @FXML
    private void handlePasswordSubmitAction(ActionEvent event){
        if(checkPass(oldPassField.getText())){
            oldPassField.setStyle("-fx-text-box-border: gray;");
            if(newPassField.getText().isEmpty()){
                errorLabelPassword.setText("new password must connect some characters");
            }
            else if(newPassField.getText().equals(confNewPassField.getText())){
                errorLabelPassword.setText("");
                newPassField.setStyle("-fx-text-box-border: gray;");
                confNewPassField.setStyle("-fx-text-box-border: gray;");
            }
            else{
                newPassField.setStyle("-fx-text-box-border: red;");
                confNewPassField.setStyle("-fx-text-box-border: red;");
                errorLabelPassword.setText("New passwords are not the same");
            }
        }
        else{
            oldPassField.setStyle("-fx-text-box-border: red;");
            if(oldPassField.getText().isEmpty()){
                errorLabelPassword.setText("You need to provide the old password");
            }
            else{
                errorLabelPassword.setText("Old password is incorrect");
            }
        }

    }
    @FXML
    private void handleEmailSubmitAction(ActionEvent event){
        if(newEmailField.getText().isEmpty()){
            errorLabelEmail.setText("new email must contain some characters");
        }
        else if(newEmailField.getText().equals(confNewEmailField.getText())){
            errorLabelEmail.setText("");
            newEmailField.setStyle("-fx-text-box-border: gray;");
            confNewEmailField.setStyle("-fx-text-box-border: gray;");
        }
        else{
            newEmailField.setStyle("-fx-text-box-border: red;");
            confNewEmailField.setStyle("-fx-text-box-border: red;");
            errorLabelEmail.setText("New emails are not the same");
        }
    }
}
