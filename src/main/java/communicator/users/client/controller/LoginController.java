package communicator.users.client.controller;

import ch.qos.logback.classic.Logger;
import communicator.users.client.Main;
import communicator.users.client.model.Model;
import communicator.users.client.model.User;
import communicator.users.client.persistence.HibernateUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class LoginController implements Controller {

    private Stage primaryStage;
    private Stage loginStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public LoginController(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }


    @Override
    public void show() {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Login.fxml"));
            loader.setController(this);
            BorderPane rootLayout = (BorderPane) loader.load();

            loginStage = new Stage();
            loginStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    HibernateUtils.shutdown();
                }
            });
            loginStage.setTitle("Login");
            loginStage.initModality(Modality.WINDOW_MODAL);
            loginStage.initOwner(primaryStage);
            Scene scene = new Scene(rootLayout);
            loginStage.setScene(scene);
            loginStage.show();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return loginStage;
    }

    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private Button loginButton;
    @FXML
    private Button registerButton;
    @FXML
    private Label warn;

    @FXML
    public void handleLoginAction(ActionEvent event) {

        boolean flag = false;
        if(passwordTextField.getText().isEmpty()){
            passwordTextField.setStyle("-fx-text-box-border: red;");
            warn.setText("Type password");
            warn.setTextFill(Color.RED);
            flag = true;
        }
        else{
            passwordTextField.setStyle("-fx-text-box-border: gray;");
        }
        if(usernameTextField.getText().isEmpty()){
            usernameTextField.setStyle("-fx-text-box-border: red;");
            warn.setText("Type username");
            warn.setTextFill(Color.RED);
            flag = true;
        }
        else{
            usernameTextField.setStyle("-fx-text-box-border: gray;");
        }
        if(flag){
            return;
        }
        User user = new User();
        user.setUsername(usernameTextField.getText());
        user.setPassword(Model.getInstance().getEncryption().encrypt(passwordTextField.getText()));
        Model.getInstance().setUser(user);
        if(Model.getInstance().authorize()) {
            Model.getInstance().setUser(user);
            loginStage.close();
            Overseer.getInstance().addController(new ActiveUsersController(primaryStage));
        }
        else{
            warn.setText("Bad Credentials");
            warn.setTextFill(Color.RED);
        }
    }

    @FXML
    public void handleRegisterAction(ActionEvent event) {
        Overseer.getInstance().addController(new RegistrationController(primaryStage));
    }
    @FXML
    public void stop() {
        HibernateUtils.shutdown();
    }

    @FXML
    public void handleEnter(ActionEvent event) {
        handleLoginAction(event);
    }

}
