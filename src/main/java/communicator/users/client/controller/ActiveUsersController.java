package communicator.users.client.controller;

import ch.qos.logback.classic.Logger;
import communicator.users.client.Main;
import communicator.users.client.model.Model;
import communicator.users.client.model.ProfileData;
import communicator.users.client.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dawid on 2015-12-05.
 */
public class ActiveUsersController implements Controller {

    private User user;
    private Stage primaryStage;
    private Stage activeUsersStage;
    private List<User> activeUsersList;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public ActiveUsersController(){}
    public ActiveUsersController(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/ActiveUsers.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            activeUsersStage = new Stage();
            activeUsersStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().logout(primaryStage);
                }
            });
            activeUsersStage.setTitle("Active Users");
            activeUsersStage.initModality(Modality.WINDOW_MODAL);
            activeUsersStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            scene.getStylesheets().add(Main.class.getResource("/list.css").toExternalForm());
            activeUsersStage.setScene(scene);
            setUsername();
            activeUsersStage.show();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return activeUsersStage;
    }
    public void setUsername(){
        usernameLabel.setText(Model.getInstance().getUserName());
        usernameLabel.setTextFill(Color.BLUE);
        usernameLabel.setFont(new Font("Arial", 30));
    }
    public void getlist(List<User> list) {
        //activeUsersList = list;
        //TODO
        //users list -> usernames


        //test list creation
        activeUsersList = new ArrayList<>();
    }
    public String getProfileUserName(){
        return userNameList.getSelectionModel().selectedItemProperty().getValue();
    }

    @FXML
    private Button optionsButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button logoutButton;
    @FXML
    private Button startConversationButton;
    @FXML
    private Button seeProfileButton;
    @FXML
    private Label usernameLabel;
    @FXML
    private Label warn;
    @FXML
    private ListView<String> userNameList;
    private ObservableList<String> userNames = FXCollections.observableArrayList();



    @FXML
    public void initialize() {
        setUserOnline();
    }

    private void setUserOnline() {
        userNames.clear();
        List<String> activeUsers = Model.getInstance().getUsersList();

        for(String user : activeUsers) {
            System.out.println(user);
            userNames.add(user);
        }

        userNameList.setItems(userNames);
    }

    @FXML
    public void handleOptionsAction(ActionEvent event) {
        Overseer.getInstance().addController(new OptionsController(primaryStage));
    }
    @FXML
    public void handleRefreshAction(ActionEvent event) {
        this.setUserOnline();
    }
    @FXML
    public void handleLogoutAction(ActionEvent event) {
        Overseer.getInstance().logout(primaryStage);
    }
    @FXML
    public void handleStartConversationAction(ActionEvent event) {
        if(userNameList.getSelectionModel().selectedItemProperty().getValue()==null){
            warn.setText("Pick a user!");
            warn.setTextFill(Color.RED);
        }
        else {
            warn.setText("");
            Model.getInstance().newConversationNotify(userNameList.getSelectionModel().selectedItemProperty().getValue());
        }
    }
    @FXML
    public void handleSeeProfileAction(ActionEvent event) {
        if(userNameList.getSelectionModel().selectedItemProperty().getValue()==null){
            warn.setText("Pick a user!");
            warn.setTextFill(Color.RED);
        }
        else {
            warn.setText("");
            ProfileData data = Model.getInstance().getProfileData(userNameList.getSelectionModel().selectedItemProperty().getValue());
            Overseer.getInstance().addController(new ProfileController(primaryStage, userNameList.getSelectionModel().selectedItemProperty().getValue(),data));
        }
    }
}
