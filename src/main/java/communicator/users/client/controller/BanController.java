package communicator.users.client.controller;

import ch.qos.logback.classic.Logger;
import communicator.users.client.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class BanController implements Controller {
    private Stage primaryStage;
    private Stage banStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public BanController(){}

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Ban.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            banStage = new Stage();
            banStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new BanController());
                }
            });
            banStage.setTitle("Ban User");
            banStage.initModality(Modality.WINDOW_MODAL);
            banStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            banStage.setScene(scene);


            banStage.showAndWait();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return banStage;
    }

    @FXML
    private Button ban;
    @FXML
    private Label userName;
    @FXML
    private TextField reasonField;
    @FXML
    private TextField expirationDateField;

    @FXML
    private void handleBanAction(ActionEvent event){

    }
}
