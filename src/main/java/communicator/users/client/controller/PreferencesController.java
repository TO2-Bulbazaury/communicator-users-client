package communicator.users.client.controller;

import ch.qos.logback.classic.Logger;
import communicator.users.client.Main;
import communicator.users.client.model.Model;
import communicator.users.client.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Created by Dawid on 2015-12-05.
 */
public class PreferencesController implements Controller {

    private Stage primaryStage;
    private Stage preferencessStage;
    private List<String> preferencesList;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());

    public PreferencesController() {

    }

    public PreferencesController(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.user = Model.getInstance().getUser();
    }


    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/Preferences.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            preferencessStage = new Stage();
            preferencessStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new PreferencesController());
                }
            });
            preferencessStage.setTitle("Preferences");
            preferencessStage.initModality(Modality.WINDOW_MODAL);
            preferencessStage.initOwner(primaryStage);

            Scene scene = new Scene(page);
            preferencessStage.setScene(scene);
            preferencessStage.showAndWait();
        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return preferencessStage;
    }

    private User user;

    @FXML
    private Button submit;
    @FXML
    private ListView<CheckBox> checkboxList;
    private ObservableList<CheckBox> checkBoxes = FXCollections.observableArrayList();


    @FXML
    void addPreference(String name) {
        CheckBox c = new CheckBox();
        c.setText(name);
        checkBoxes.add(c);
    }

    @FXML
    public void initialize() {
        getAvailablePlugins();


        addPreference("P1 - uruchamia archiwum");
        addPreference("P2 - uruchamia podstawowe emotikony");


        checkboxList.setItems(checkBoxes);
    }



    @FXML
    public void handleSubmitAction(ActionEvent event) {
        for(CheckBox c: checkBoxes){
            if(c.isSelected()) {
                /* TODO
                * adding preferences to user
                * and saving them into database*/
            }

        }
//        for(CheckBox c: checkBoxes) {
//            Preference preference = new Preference(user, c.getText(), c.isSelected());
//            user.addPreference(preference);
//        }
//        Model.getInstance().getDatabaseConnector().savePreferences(user);
    }

    public void getAvailablePlugins(){

    }

}
