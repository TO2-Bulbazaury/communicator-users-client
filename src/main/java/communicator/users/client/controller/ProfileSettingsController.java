package communicator.users.client.controller;

import ch.qos.logback.classic.Logger;
import communicator.users.client.Main;
import communicator.users.client.model.Model;
import communicator.users.client.model.ProfileData;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dawid on 2015-12-05.
 */
public class ProfileSettingsController implements Controller {
    private Stage primaryStage;
    private Stage profileSettingsStage;
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass().toString());
    private ProfileData profileData;

    public ProfileSettingsController(){}
    public ProfileSettingsController(Stage primaryStage) {

    }

    @Override
    public void show() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/ProfileSettings.fxml"));
            loader.setController(this);
            BorderPane page = (BorderPane) loader.load();

            profileSettingsStage = new Stage();
            profileSettingsStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    Overseer.getInstance().closingWindow(new ProfileSettingsController());
                }
            });
            profileSettingsStage.setTitle("Profile settings");
            profileSettingsStage.initModality(Modality.WINDOW_MODAL);
            profileSettingsStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            profileSettingsStage.setScene(scene);


            profileSettingsStage.showAndWait();

        } catch (IOException e) {
            logger.error(e.toString());
        }
    }

    @Override
    public Stage getStage() {
        return profileSettingsStage;
    }

    @FXML
    private TextField nameField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField ageField;
    @FXML
    private CheckBox nameCheck;
    @FXML
    private CheckBox cityCheck;
    @FXML
    private CheckBox ageCheck;
    @FXML
    private Button submit;
    @FXML
    private Label username;

    @FXML
    public void initialize(){
        username.setText(Model.getInstance().getUserName());
        profileData = Model.getInstance().getProfileData(Model.getInstance().getUserName());
        nameField.setText(profileData.getName());
        cityField.setText(profileData.getCity());
        ageField.setText(profileData.getAge());
    }
    @FXML
    public void handleSubmitAction(ActionEvent event){
        profileData.setName(nameField.getText());
        profileData.setCity(cityField.getText());
        profileData.setAge(ageField.getText());
    }
}
